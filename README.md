# Sorting Service
This application is a service which goal is to sort collection of books containing an `id`, `title`, `author` and an `editionYear`.

***

## Installation
In order to correctly run this application, you first need to provide a functional Java� 8 installation ([here's how](https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html)).

***

## Running
Once you have Java� 8 successfully installed, we're good to start playing with the sorting service.

Here's the run command anatomy:
~~~~
java -jar sorting-service.jar BOOKS_JSON_PATH [-arg1 value1 [-arg2 value2 [-arg3 value3]]]
~~~~

Where `BOOKS_JSON_PATH` is a path pointing to a JSON file containing our collection of books which should look like this:
~~~~
[
	{
		id: 1,
		title: 'A Nice Book Title',
		author: 'Some Clever Author',
		editionYear: 2005
	},
	{
		id: 2,
		title: 'Another Nice Book Title',
		author: 'Its Author',
		editionYear: 2002
	},
	...
]
~~~~

As for the other arguments, you can specify which books attributes are going to be considered for sorting and their directions (ascending or descending). These are the possible values:

| Argument | Possible values |
| -------- | ---------- |
| `-title` | `asc` or `desc` |
| `-author` | `asc` or `desc` |
| `-editionYear` | `asc` or `desc` |

You can easily get some help by running the following command:
~~~~
java -jar sorting-service.jar -help
~~~~

***

## Some Examples
These are the expected results for this example book collection:

### Collection (available [here](https://bitbucket.org/RaphaSS/sorting-service/raw/36fcece80eaa48ce78519143184d635d44990a23/src/test/resources/book_set.json))
~~~~
[
	{
		id: 1,
		title: 'Java How to Program',
		author: 'Deitel & Deitel',
		editionYear: 2007
	},
	{
		id: 2,
		title: 'Patterns of Enterprise Application Architecture',
		author: 'Martin Fowler',
		editionYear: 2002
	},
	{
		id: 3,
		title: 'Head First Design Patterns',
		author: 'Elisabeth Freeman',
		editionYear: 2004
	},
	{
		id: 4,
		title: 'Internet & World Wide Web: How to Program',
		author: 'Deitel & Deitel',
		editionYear: 2007
	}
]
~~~~

### Execution 1
Assuming the following command
~~~~
java -jar sorting-service.jar /path/to/book_set.json -title asc
~~~~

The `id`s of the resulting sequence are going to be `3, 4, 1 and 2`.

### Execution 2
Assuming the following command
~~~~
java -jar sorting-service.jar /path/to/book_set.json -title desc -author asc
~~~~

The `id`s of the resulting sequence are going to be `1, 4, 3 and 2`.

### Execution 3
Assuming the following command
~~~~
java -jar sorting-service.jar /path/to/book_set.json -editionYear desc -title asc -author -desc
~~~~

The `id`s of the resulting sequence are going to be `4, 1, 3 and 2`.

### Execution 4
Assuming the following command
~~~~
java -jar sorting-service.jar /path/to/book_set.json
~~~~

There will be no resulting collection.