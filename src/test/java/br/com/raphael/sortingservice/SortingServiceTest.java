package br.com.raphael.sortingservice;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import br.com.raphael.sortingservice.exception.SortingServiceException;
import br.com.raphael.sortingservice.model.Book;
import br.com.raphael.sortingservice.service.SortingService;
import br.com.raphael.sortingservice.service.SortingServiceImpl;
import br.com.raphael.sortingservice.util.sorting.SortingDirection;
import br.com.raphael.sortingservice.util.sorting.SortingParametersList;
import br.com.raphael.sortingservice.util.sorting.parameters.impl.Author;
import br.com.raphael.sortingservice.util.sorting.parameters.impl.EditionYear;
import br.com.raphael.sortingservice.util.sorting.parameters.impl.Title;

public class SortingServiceTest {

	private static final String BOOK_SET_PATH = "book_set.json";

	private static SortingService sortingService;

	private static List<Book> books;

	@BeforeClass
	public static void init() {
		sortingService = new SortingServiceImpl();

		final ClassLoader classLoader = SortingServiceTest.class.getClassLoader();
		try {
			final String result = IOUtils.toString(classLoader.getResourceAsStream(BOOK_SET_PATH), Charset.defaultCharset());
			books = new Gson().fromJson(result, new TypeToken<List<Book>>() {}.getType());
		} catch(final IOException e) {
			e.printStackTrace();
		}
	}

	private static void assertEqualsIds(final List<Book> actual, final List<Integer> ids) {
		Assert.assertTrue(CollectionUtils.isEqualCollection(getBooksIds(actual), ids));
	}

	private static List<Integer> getBooksIds(final List<Book> books) {
		final List<Integer> ids = new ArrayList<>();
		for(final Book book : books) {
			ids.add(book.getId());
		}
		return ids;
	}

	@Test
	public void ordenarLivrosPorTituloAsc() {
		final SortingParametersList params = new SortingParametersList.Builder()
			.setTitle(new Title(SortingDirection.ASCENDING))
			.build();

		try {
			final List<Book> sortedBooks = sortingService.sort(books, params);
			assertEqualsIds(sortedBooks, Arrays.asList(3, 4, 1, 2));
		} catch(final SortingServiceException e) {
			Assert.fail();
		}
	}

	@Test
	public void ordenarLivrosPorAutorAscTitulosDesc() {
		final SortingParametersList params = new SortingParametersList.Builder()
			.setTitle(new Title(SortingDirection.DESCENDING))
			.setAuthor(new Author(SortingDirection.ASCENDING))
			.build();

		try {
			final List<Book> sortedBooks = sortingService.sort(books, params);
			assertEqualsIds(sortedBooks, Arrays.asList(1, 4, 3, 2));
		} catch(final SortingServiceException e) {
			Assert.fail();
		}
	}

	@Test
	public void ordenarLivrosPorEdicaoDescAuthorDescTituloAsc() {
		final SortingParametersList params = new SortingParametersList.Builder()
			.setTitle(new Title(SortingDirection.ASCENDING))
			.setAuthor(new Author(SortingDirection.DESCENDING))
			.setEditionYear(new EditionYear(SortingDirection.DESCENDING))
			.build();

		try {
			final List<Book> sortedBooks = sortingService.sort(books, params);
			assertEqualsIds(sortedBooks, Arrays.asList(4, 1, 3, 2));
		} catch(final SortingServiceException e) {
			Assert.fail();
		}
	}

	@Test(expected = SortingServiceException.class)
	public void ordenarLivrosComParametrosNull() throws SortingServiceException {
		sortingService.sort(books, null);
	}

	@Test
	public void ordenarLivrosSemParametros() {
		final SortingParametersList params = new SortingParametersList.Builder()
			.build();

		try {
			final List<Book> sortedBooks = sortingService.sort(books, params);
			assertEqualsIds(sortedBooks, new ArrayList<>());
		} catch(final SortingServiceException e) {
			Assert.fail();
		}
	}

}
