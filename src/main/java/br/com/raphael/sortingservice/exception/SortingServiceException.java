package br.com.raphael.sortingservice.exception;

public class SortingServiceException extends Exception {

	private static final long serialVersionUID = 2407025809786096029L;

	public SortingServiceException() {
		super("Os parametros de ordenacao devem ser especificados!");
	}

}
