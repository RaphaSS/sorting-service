package br.com.raphael.sortingservice.service;

import java.util.ArrayList;
import java.util.List;

import br.com.raphael.sortingservice.exception.SortingServiceException;
import br.com.raphael.sortingservice.model.Book;
import br.com.raphael.sortingservice.util.sorting.SortingParametersList;

public class SortingServiceImpl implements SortingService {

	@Override
	public List<Book> sort(final List<Book> books, final SortingParametersList sortingParameters) throws SortingServiceException {
		if(sortingParameters == null)
			throw new SortingServiceException();

		if(sortingParameters.getTitle() == null && sortingParameters.getAuthor() == null && sortingParameters.getEditionYear() == null)
			return new ArrayList<>();

		final List<Book> sortedBooks = new ArrayList<>(books);

		if(sortingParameters.getTitle() != null) {
			sortedBooks.sort(sortingParameters.getTitle().getComparator());
		}

		if(sortingParameters.getAuthor() != null) {
			sortedBooks.sort(sortingParameters.getAuthor().getComparator());
		}

		if(sortingParameters.getEditionYear() != null) {
			sortedBooks.sort(sortingParameters.getEditionYear().getComparator());
		}

		return sortedBooks;
	}

}
