package br.com.raphael.sortingservice.service;

import java.util.List;

import br.com.raphael.sortingservice.exception.SortingServiceException;
import br.com.raphael.sortingservice.model.Book;
import br.com.raphael.sortingservice.util.sorting.SortingParametersList;

public interface SortingService {

	List<Book> sort(List<Book> books, SortingParametersList sortingParameters) throws SortingServiceException;

}
