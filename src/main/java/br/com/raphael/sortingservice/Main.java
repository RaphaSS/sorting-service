package br.com.raphael.sortingservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import br.com.raphael.sortingservice.exception.SortingServiceException;
import br.com.raphael.sortingservice.model.Book;
import br.com.raphael.sortingservice.service.SortingService;
import br.com.raphael.sortingservice.service.SortingServiceImpl;
import br.com.raphael.sortingservice.util.run.ArgumentParser;
import br.com.raphael.sortingservice.util.sorting.SortingParametersList;
import br.com.raphael.sortingservice.util.sorting.parameters.SortingParameter;
import br.com.raphael.sortingservice.util.sorting.parameters.factory.SortingParameterFactory;
import br.com.raphael.sortingservice.util.sorting.parameters.impl.Author;
import br.com.raphael.sortingservice.util.sorting.parameters.impl.EditionYear;
import br.com.raphael.sortingservice.util.sorting.parameters.impl.Title;

public class Main {

	public static void main(final String[] args) {
		final String arg1 = args[0];
		if(arg1.equals("-help"))
			printHelp();
		else {
			final String filePath = arg1;
			final File file = new File(filePath);
			try {
				final SortingParametersList.Builder builder = new SortingParametersList.Builder();
				if(args.length > 1) {
					// Reads the parameter-values pairs
					for(int i = 1; i < args.length; i += 2) {
						final String argument = args[i];
						if(ArgumentParser.isArgumentValid(argument)) {
							final String parameter = ArgumentParser.getParameterName(argument);
							final String sortingDirection = args[i + 1];
							final SortingParameter sortingParameter = SortingParameterFactory.getSortingParameter(parameter, sortingDirection);
							if(sortingParameter instanceof Title) {
								builder.setTitle((Title) sortingParameter);
							} else if(sortingParameter instanceof Author) {
								builder.setAuthor((Author) sortingParameter);
							} else {
								builder.setEditionYear((EditionYear) sortingParameter);
							}
						} else {
							// If the syntax is incorrect, prints help
							throw new Exception();
						}
					}
				}

				final SortingParametersList sortingParametersList = builder.build();
				System.out.println("Sorting Parameters");
				printParams(sortingParametersList);

				final String json = IOUtils.toString(new FileInputStream(file), Charset.defaultCharset());
				final Gson gson = new Gson();
				final List<Book> books = gson.fromJson(json, new TypeToken<List<Book>>() {}.getType());

				System.out.println("\nLoaded books");
				printBooks(books);

				final SortingService sortingService = new SortingServiceImpl();
				final List<Book> sortedBooks = sortingService.sort(books, sortingParametersList);

				System.out.println("\nSorted books");
				printBooks(sortedBooks);
			} catch(final IOException | SortingServiceException e) {
				e.printStackTrace();
			} catch(final Exception e) {
				printHelp();
			}
		}
	}

	private static void printHelp() {
		System.out.println("User guide");
		System.out.println("------------------------------");
		System.out.println("Command anatomy");
		System.out.println("java -jar sorting-service.jar BOOKS_JSON_PATH [-arg1 value1 [-arg2 value2 [-arg3 value3]]]");
		System.out.println("------------------------------");
		System.out.println("Optional parameters");
		System.out.println("-title\t\tasc|desc\tWhether to sort the books by their titles in ascending or descending order");
		System.out.println("-author\t\tasc|desc\tWhether to sort the books by their author(s) name in ascending or descending order");
		System.out.println("-editionYear\tasc|desc\tWhether to sort the books by their edition year in ascending or descending order");
		System.out.println("------------------------------");
		System.exit(0);
	}

	private static void printParams(final SortingParametersList sortingParametersList) {
		final Title title = sortingParametersList.getTitle();
		if(title != null) {
			System.out.println("------------------------------");
			System.out.println("Parameter: Title");
			System.out.println("Order: " + title.getSortingDirection());
		}

		final Author author = sortingParametersList.getAuthor();
		if(author != null) {
			System.out.println("------------------------------");
			System.out.println("Parameter: Author");
			System.out.println("Order: " + author.getSortingDirection());
		}

		final EditionYear editionYear = sortingParametersList.getEditionYear();
		if(editionYear != null) {
			System.out.println("------------------------------");
			System.out.println("Parameter: EditionYear");
			System.out.println("Order: " + editionYear.getSortingDirection());
		}
		System.out.println("------------------------------");
	}

	private static void printBooks(final List<Book> books) {
		for(final Book book : books) {
			System.out.println("------------------------------");
			System.out.println("ID: " + book.getId());
			System.out.println("Title: " + book.getTitle());
			System.out.println("Author: " + book.getAuthor());
			System.out.println("Edition Year: " + book.getEditionYear());
		}
		System.out.println("------------------------------");
	}

}
