package br.com.raphael.sortingservice.util.run;

import br.com.raphael.sortingservice.util.sorting.parameters.impl.Author;
import br.com.raphael.sortingservice.util.sorting.parameters.impl.EditionYear;
import br.com.raphael.sortingservice.util.sorting.parameters.impl.Title;

public class ArgumentParser {

	/**
	 * Tests whether the argument passed is valid or not
	 * 
	 * @param arg
	 *            Command line argument (preceeded by the "<code>-</code>" char)
	 * @return <code>true</code> if argument equals to <code>-title</code>,
	 *         <code>-author</code> or <code>-editionYear</code> or
	 *         <code>false</code> if it doesn't
	 */
	public static boolean isArgumentValid(final String arg) {
		return arg.equals("-".concat(Title.NAME)) || arg.equals("-".concat(Author.NAME)) || arg.equals("-".concat(EditionYear.NAME));
	}

	/**
	 * Get the actual parameter name (without the "<code>-<code>" char)
	 * @param arg Command line argument (preceeded by the "<code>-</code>" char)
	 * 
	 * @return the parameter name without the "<code>-<code>" char
	 */
	public static String getParameterName(final String arg) {
		return arg.substring(1);
	}

}
