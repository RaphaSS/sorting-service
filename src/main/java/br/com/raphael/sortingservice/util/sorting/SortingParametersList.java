package br.com.raphael.sortingservice.util.sorting;

import br.com.raphael.sortingservice.util.sorting.parameters.impl.Author;
import br.com.raphael.sortingservice.util.sorting.parameters.impl.EditionYear;
import br.com.raphael.sortingservice.util.sorting.parameters.impl.Title;

public class SortingParametersList {

	private Title title;

	private Author author;

	private EditionYear editionYear;

	private SortingParametersList() {}

	public Title getTitle() {
		return title;
	}

	public void setTitle(final Title title) {
		this.title = title;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(final Author author) {
		this.author = author;
	}

	public EditionYear getEditionYear() {
		return editionYear;
	}

	public void setEditionYear(final EditionYear editionYear) {
		this.editionYear = editionYear;
	}

	public static class Builder {

		private final SortingParametersList sortingParametersList;

		public Builder() {
			sortingParametersList = new SortingParametersList();
		}

		public SortingParametersList build() {
			return sortingParametersList;
		}

		public Builder setTitle(final Title title) {
			sortingParametersList.setTitle(title);
			return this;
		}

		public Builder setAuthor(final Author author) {
			sortingParametersList.setAuthor(author);
			return this;
		}

		public Builder setEditionYear(final EditionYear editionYear) {
			sortingParametersList.setEditionYear(editionYear);
			return this;
		}

	}

}
