package br.com.raphael.sortingservice.util.sorting.parameters.impl;

import java.util.Comparator;

import br.com.raphael.sortingservice.model.Book;
import br.com.raphael.sortingservice.util.sorting.SortingDirection;
import br.com.raphael.sortingservice.util.sorting.parameters.SortingParameter;

public class EditionYear extends SortingParameter {

	public static final String NAME = "editionYear";

	public EditionYear(final SortingDirection sortingDirection) {
		super(NAME, sortingDirection);
	}

	@Override
	public Comparator<Book> getComparator() {
		if(SortingDirection.ASCENDING.equals(getSortingDirection()))
			return (o1, o2) -> {
				return o1.getEditionYear().compareTo(o2.getEditionYear());
			};
		return (o1, o2) -> {
			return o2.getEditionYear().compareTo(o1.getEditionYear());
		};
	}

}
