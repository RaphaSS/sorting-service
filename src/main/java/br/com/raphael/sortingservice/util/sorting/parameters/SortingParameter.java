package br.com.raphael.sortingservice.util.sorting.parameters;

import java.util.Comparator;

import br.com.raphael.sortingservice.model.Book;
import br.com.raphael.sortingservice.util.sorting.SortingDirection;

public abstract class SortingParameter {

	private String name;

	private SortingDirection sortingDirection;

	protected SortingParameter(final String name, final SortingDirection sortingDirection) {
		this.name = name;
		this.sortingDirection = sortingDirection;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public SortingDirection getSortingDirection() {
		return sortingDirection;
	}

	public void setSortingDirection(final SortingDirection sortingDirection) {
		this.sortingDirection = sortingDirection;
	}

	public abstract Comparator<Book> getComparator();

}
