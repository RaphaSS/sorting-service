package br.com.raphael.sortingservice.util.sorting.parameters.factory;

import br.com.raphael.sortingservice.util.sorting.SortingDirection;
import br.com.raphael.sortingservice.util.sorting.parameters.SortingParameter;
import br.com.raphael.sortingservice.util.sorting.parameters.impl.Author;
import br.com.raphael.sortingservice.util.sorting.parameters.impl.EditionYear;
import br.com.raphael.sortingservice.util.sorting.parameters.impl.Title;

public class SortingParameterFactory {

	@SuppressWarnings("unchecked")
	public static <T extends SortingParameter> T getSortingParameter(final String parameter, final String sortingDirection) {
		SortingParameter sortingParameter;
		final SortingDirection direction = SortingDirection.fromCommandLineParameter(sortingDirection);
		switch(parameter) {
			case Title.NAME:
				sortingParameter = new Title(direction);
				break;
			case Author.NAME:
				sortingParameter = new Author(direction);
				break;
			case EditionYear.NAME:
				sortingParameter = new EditionYear(direction);
				break;

			default:
				return null;
		}
		return (T) sortingParameter;
	}

}
