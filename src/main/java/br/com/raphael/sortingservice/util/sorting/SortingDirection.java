package br.com.raphael.sortingservice.util.sorting;

public enum SortingDirection {

	ASCENDING, DESCENDING;

	public static SortingDirection fromCommandLineParameter(final String parameter) {
		switch(parameter) {
			case "asc":
				return ASCENDING;
			case "desc":
				return DESCENDING;

			default:
				return null;
		}
	}

}
