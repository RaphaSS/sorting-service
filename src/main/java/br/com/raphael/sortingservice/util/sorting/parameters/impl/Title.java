package br.com.raphael.sortingservice.util.sorting.parameters.impl;

import java.util.Comparator;

import br.com.raphael.sortingservice.model.Book;
import br.com.raphael.sortingservice.util.sorting.SortingDirection;
import br.com.raphael.sortingservice.util.sorting.parameters.SortingParameter;

public class Title extends SortingParameter {

	public static final String NAME = "title";

	public Title(final SortingDirection sortingDirection) {
		super(NAME, sortingDirection);
	}

	@Override
	public Comparator<Book> getComparator() {
		if(SortingDirection.ASCENDING.equals(getSortingDirection()))
			return (o1, o2) -> {
				return o1.getTitle().compareTo(o2.getTitle());
			};
		return (o1, o2) -> {
			return o2.getTitle().compareTo(o1.getTitle());
		};
	}

}
