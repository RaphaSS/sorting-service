package br.com.raphael.sortingservice.util.sorting.parameters.impl;

import java.util.Comparator;

import br.com.raphael.sortingservice.model.Book;
import br.com.raphael.sortingservice.util.sorting.SortingDirection;
import br.com.raphael.sortingservice.util.sorting.parameters.SortingParameter;

public class Author extends SortingParameter {

	public static final String NAME = "author";

	public Author(final SortingDirection sortingDirection) {
		super(NAME, sortingDirection);
	}

	@Override
	public Comparator<Book> getComparator() {
		if(SortingDirection.ASCENDING.equals(getSortingDirection()))
			return (o1, o2) -> {
				return o1.getAuthor().compareTo(o2.getAuthor());
			};
		return (o1, o2) -> {
			return o2.getAuthor().compareTo(o1.getAuthor());
		};
	}

}
