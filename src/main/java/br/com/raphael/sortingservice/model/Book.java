package br.com.raphael.sortingservice.model;

public class Book {

	private Integer id;

	private String title;

	private String author;

	private Integer editionYear;

	public Integer getId() {
		return id;
	}

	public void setId(final Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(final String author) {
		this.author = author;
	}

	public Integer getEditionYear() {
		return editionYear;
	}

	public void setEditionYear(final Integer editionYear) {
		this.editionYear = editionYear;
	}

}
